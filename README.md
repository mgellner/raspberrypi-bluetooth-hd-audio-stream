# Raspberry Pi Bluetooth HD audio stream

Configure Raspberry Pi 4 with Raspbian 11 (bullseye) as an HD bluetooth audio sink for streaming from mobile phones. No external bluetooth adapter or sound card needed, if the raspi is connected via HDMI. As OS we use Raspberry Pi OS Lite (64 bit).

This repository is based on the work of https://github.com/fdanis-oss/pw_wp_bluetooth_rpi_speaker

## Backup your SD card

```bash
sudo dd bs=4M if=/dev/sdX | gzip > raspi4.img.gz
```

## Restore if needed

```bash
sudo gunzip --stdout raspi4.img.gz | sudo dd bs=4M of=/dev/sdX
```

## Prepare the interfaces

Disable wifi as it may interfere with the built-in bluetooth adapter. To do so, add "disable-wifi" to dtoverlay in your */boot/config.txt*

```bash
[all]
dtoverlay=vc4-kms-v3d,disable-wifi
```

If you don't use the headphone jack disable it by commenting it out, as it may cause issues with the default audio sink

```bash
#dtparam=audio=on
```

## Install pipewire and wireplumber with support for HD audio codecs such as aptX HD from debian testing

> :warning: Even though the stable release has a higher priority than testing, this may have unwanted [side effects](https://unix.stackexchange.com/q/647204)

```bash
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 648ACFD622F3D138
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 0E98404D386FA1D9
echo 'APT::Default-Release "stable";' | sudo tee /etc/apt/apt.conf.d/99defaultrelease
echo "deb http://ftp.de.debian.org/debian/ testing main contrib non-free" | sudo tee /etc/apt/sources.list.d/testing.list
sudo apt update
sudo apt -t testing install pipewire wireplumber libspa-0.2-bluetooth
```

## Install the bluetooth agent

```bash
sudo apt install python3-dbus
mkdir -p ~/.config/systemd/user/
cd ~
wget https://raw.githubusercontent.com/fdanis-oss/pw_wp_bluetooth_rpi_speaker/master/speaker-agent.py
cd ~/.config/systemd/user/
wget https://raw.githubusercontent.com/fdanis-oss/pw_wp_bluetooth_rpi_speaker/master/.config/systemd/user/speaker-agent.service
systemctl --user enable speaker-agent.service
sudo sed -i 's/#JustWorksRepairing.*/JustWorksRepairing = always/' /etc/bluetooth/main.conf
sudo reboot
```

## Set HDMI as the default audio sink for pipewire

For that we use the pulseaudio-utils, as they also work for pipewire

```bash
sudo apt install pulseaudio-utils
pactl set-default-sink $(pactl list sinks|grep Name|cut -d' ' -f2)
```

## Troubleshooting

If you have problems with initial pairing, manually trust your device first.

```bash
sudo bluetoothctl
default-agent
discoverable on
# connect your phone via bluetooth, confirm, authorize
trust XX:XX:XX:XX:XX:XX  # enable auto-reconnect
exit
```